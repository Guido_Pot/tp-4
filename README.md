# TP4
Este repositorio tiene la resolucion del ejercicio a entregar de la pracitica 4 de Sistemas Operativos Y Redes [(E1224 / E0224)](https://www1.ing.unlp.edu.ar/catedras/E0224/)

El grupo 5 esta formado por los integrantes:

|Nombre|Numero de Alumno|
|------|----------------|
|Ireba Facundo|73611/4|
|Martinez Navamuel Manuel|71319/4|
|Potente Guido|73230/5|

## Consigna

Se desea implementar un buffer `PING-PONG` para que dos procesos generen datos y otro los consuma. La implementación debe utilizar un archivo para la obtención de datos producidos y otro para almacenar los datos leídos.
Los procesos productores deben leer los datos desde un archivo y escribirlos en un buffer ping-pong, en donde cada dato será una línea de texto. Para diferenciar el texto producido por cada uno la cadena debe iniciar con `P1:` o `P2:` según quien sea el que generó el dato. El productor deberá leer las líneas del archivo que contengan texto y producir de a una línea por segundo.
Solo deberá ejecutarse un productor a la vez, para cambiar de proceso productor se deberá enviar una señal que permitirá pasar de un productor a otro.
El proceso consumidor debe leer desde el buffer ping-pong e imprimir los valores leídos en pantalla. Además, debe almacenarlos en otro archivo.
El problema debe implementarse usando hilos, variables compartidas y mutex.

## Paquetes necesarios

Si se quiere compilar el codigo es necesario tener gcc o algun compilador de `C`.
El siguiente comando permite verificar si tiene gcc instalado:

```Console
gcc -v
```

En caso de no tenerlo, verifique el nombre del paquete para su respectiva distribucion.

Para compilar el codigo ejecute:

```Console
gcc Ejercicio.c -o <NOMBRE DEL EJECUTABLE> -lpthread
```

## Uso del programa

Sea que use el ejecutable incluido o el suyo, para comenzar el programa ponga:

```Console
./Ejecutable
```

En caso de que sea exitoso, comenzara a ver las lineas generadas por el programa desde el productor.

Se incluyeron dos señales para comandar el comportamiento (`SIGUSR1` y `SIGTERM`) que permiten cambiar el productor, y teminar el programa para poder escribir en el archivo de salida `archivo_que_escribo.txt`.
Para mandar estas señales observe el PID que replica constantemente el programa y ejecute alguna de la siguientes lineas:

```Console
kill -SIGUSR1 <PID>  # Para cambiar de productor
kill -SIGTERM <PID>  # Para terminar la ejecucion
```

> El cambio de productor se da solo cuando termina alguno de los dos buffers.
> El archivo de escritura es sobreescrito, o creado si no existe, en cada ejecucion del programa.
