#include <semaphore.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h> 
#include <errno.h>
#include <string.h> 
#include <unistd.h> 
#include <fcntl.h> 
#include <signal.h>


#define LINEAS 10
#define CARACTERES_POR_LINEA 200


pthread_t hilo_productor_1,hilo_productor_2, hilo_consumidor; 
int flag_escritura_b1=1;
int flag_escritura_b2=0;
int flag_lectura_b1=0;
int flag_lectura_b2=0;
int flag_de_cambio=0;
int flag_de_cambio_2=0;

pthread_mutex_t lock;

int P1=1;
int P2=2;

int posicion=0;

FILE *file_que_leo;
FILE *file_que_escribo;
int i_productor=0;
int i_consumidor=0;

char buffer[LINEAS][CARACTERES_POR_LINEA];
char string_auxiliar[CARACTERES_POR_LINEA-3];

void handler_term(int);
void handler_USR1(int);

void *escritura_buffer(void *P) 
{   
    pthread_mutex_lock(&lock);
    while(1)
    {   
        //INICIO DE PRODUCTOR 1//

        if(*(int*)P==1 && flag_de_cambio==0)
        {
            

            //ESCRITURA DE BUFFER 1//
            if(flag_escritura_b1==1 && flag_lectura_b1==0 && flag_escritura_b2==0)
            {
                printf("Soy el Productor 1, hilo %lX del proceso %d y voy a escribir el buffer 1.\n\n",pthread_self(),getpid());
                
                

                while(i_productor<(LINEAS/2))
                {
                    strcpy(buffer[i_productor],"P1:");

                    fgets(string_auxiliar,(CARACTERES_POR_LINEA-3),file_que_leo);
                    strcat(buffer[i_productor],string_auxiliar);
                    printf("%s\n",buffer[i_productor]);
                    sleep(1);
                    i_productor++;
                }
                        
            
                printf("He escrito Buffer 1\n\n");


                flag_lectura_b1=1;
                flag_escritura_b1=0;
                flag_escritura_b2=1;
                

            }

            //ESCRITURA DE BUFFER 2//
            if(flag_escritura_b2==1 && flag_lectura_b2==0 && flag_escritura_b1==1)
            {
                printf("Soy el Productor 1, hilo %lX del proceso %d y voy a escribir el buffer 2.\n\n",pthread_self(),getpid());
                

                while(i_productor>=(LINEAS/2) && i_productor<LINEAS)
                {

                    strcpy(buffer[i_productor],"P1:");

                    fgets(string_auxiliar,(CARACTERES_POR_LINEA-3),file_que_leo);
                    strcat(buffer[i_productor],string_auxiliar);
                    printf("%s\n",buffer[i_productor]);
                    sleep(1);
                    i_productor++;
                }
                i_productor=0;
                printf("He escrito Buffer 2\n\n");


                flag_lectura_b2=1;
                flag_escritura_b2=0;
                flag_escritura_b1=1;

            }

        }
        //FIN DE PRODUCTOR 1//


        //INICIO DE PRODCUTOR 2//
        if(*(int*)P==2 && flag_de_cambio==1)
        {
            

            //ESCRITURA BUFFER 1//
            if(flag_escritura_b1==1 && flag_lectura_b1==0 && flag_escritura_b2==0)
            {
                printf("Soy el Productor 2, hilo %lX del proceso %d y voy a escribir el buffer 1.\n\n",pthread_self(),getpid());
                


                while(i_productor<(LINEAS/2))
                {
                    strcpy(buffer[i_productor],"P2:");
                
                    fgets(string_auxiliar,(CARACTERES_POR_LINEA-3),file_que_leo);
                    strcat(buffer[i_productor],string_auxiliar);
                    printf("%s\n",buffer[i_productor]);
                    sleep(1);
                    i_productor++;
                }
                        
            
                printf("He escrito Buffer 1\n\n");


                flag_lectura_b1=1;
                flag_escritura_b1=0;
                flag_escritura_b2=1;

            }

            //ESCRITURA BUFFER 2//
            if(flag_escritura_b2==1 && flag_lectura_b2==0 && flag_escritura_b1==1)
            {
                printf("Soy el Productor 2, hilo %lX del proceso %d y voy a escribir el buffer 1.\n\n",pthread_self(),getpid());
                


                while(i_productor>=(LINEAS/2) && i_productor<LINEAS)
                {

                    strcpy(buffer[i_productor],"P2:");
                
                    fgets(string_auxiliar,(CARACTERES_POR_LINEA-3),file_que_leo);
                    strcat(buffer[i_productor],string_auxiliar);
                    printf("%s\n",buffer[i_productor]);
                    sleep(1);
                    i_productor++;
                }
                i_productor=0;
                printf("He escrito Buffer 2\n\n");


                flag_lectura_b2=1;
                flag_escritura_b2=0;
                flag_escritura_b1=1;

            }

        }
        //FIN DE PRODUCTOR 2

        if(flag_de_cambio_2==1)
        {
            pthread_mutex_unlock(&lock);
            flag_de_cambio_2=0;
        }
    }
}

void *lectura_buffer(void *arg) 
{
    while(1)
    {
        if(flag_lectura_b1==1 && flag_lectura_b2==0)
        {
            printf("Soy el Consumidor, hilo %lX del proceso %d, y voy a leer el buffer 1...\n\n",pthread_self(),getpid());


            while(i_consumidor<(LINEAS/2))
            {
                fprintf(file_que_escribo,"%s",buffer[i_consumidor]);
                i_consumidor++;
            }
            printf("He leido Buffer 1\n\n");

            
            flag_lectura_b1=0;
            flag_escritura_b1=1;


        }

        if(flag_lectura_b2==1 && flag_lectura_b1==0)
        {
            printf("Soy el Consumidor, hilo %lX del proceso %d, y voy a leer el buffer 2...\n\n",pthread_self(),getpid());


            while(i_consumidor>=(LINEAS/2) && i_consumidor<LINEAS)
            {
                fprintf(file_que_escribo,"%s",buffer[i_consumidor]);
                i_consumidor++;
            }
            i_consumidor=0;
            printf("He leido Buffer 2\n\n");


            flag_lectura_b2=0;
            flag_escritura_b2=1;


        }
        


    }
}

int main(void) 
{

    printf("\nPID del proceso: %d\n",getpid());

    signal(SIGTERM,handler_term);
    signal(SIGUSR1,handler_USR1);
    
    file_que_leo=fopen("archivo_que_leo.txt","r");
    file_que_escribo=fopen("archivo_que_escribo.txt","w+");

    pthread_mutex_init(&lock,NULL);

    pthread_create(&hilo_consumidor,NULL,lectura_buffer,NULL);
    pthread_create(&hilo_productor_1, NULL, escritura_buffer, &P1);
    sleep(1);
    pthread_create(&hilo_productor_2, NULL, escritura_buffer, &P2);

    pthread_join(hilo_productor_1,NULL);
    pthread_join(hilo_productor_2,NULL);



    while(1)
    {

    }

    return 0;
}

void handler_term(int sig)
{
    fclose(file_que_leo);
    fclose(file_que_escribo);
    puts("Se ha finalizado el programa.");
    exit(1);
}

void handler_USR1(int sig)
{
    flag_de_cambio_2=1;
    if(flag_de_cambio==0)
    {
        flag_de_cambio=1;
    }
    else if(flag_de_cambio==1)
    {
        flag_de_cambio=0;
    }

}